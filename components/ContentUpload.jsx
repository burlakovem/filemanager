/**
 * Content upload
 */

import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {uploadedFile, fileUploaded, goToPage, selectedFile, notUploadedFile} from '../actions/index';


class ContentUpload extends React.Component{

    constructor(props){
        super();
        this.refInputUpload = React.createRef();
        this.refLoadArea = React.createRef();
        this.addUploadFiles = this.addUploadFiles.bind(this);
        this.changeFiles = this.changeFiles.bind(this);
        this.dragOver = this.dragOver.bind(this);
        this.dragLeave = this.dragLeave.bind(this);
        this.drop = this.drop.bind(this);
    }

    /**
     * Simulate a click
     */
    addUploadFiles(){
        $(this.refInputUpload.current).click();
    }

    /**
     * Getting allowed file types
     * @return {*}
     */
    getFileTypes(){
        if(this.props.fileTypes === 'any') return window.APP.allowedUpload;
        if(typeof this.props.fileTypes == 'string'){
            return this.props.fileTypes;
        }else{
            return this.props.fileTypes.join(', ');
        }
    }

    /**
     * Uploading file
     * @param input
     */
    changeFiles(input = []){
        var self = this;
        var files = (input.length) ? input : this.refInputUpload.current.files;
        var date = new Date();
        var time = date.getTime();

        function upload(file, key = 0){

            var id = time + '_' + Math.round(Math.random() * 100) + '_' + key;

            if(self.getFileTypes().search(file.type) == -1)
                return window.modal.openError('Файл ' + file.name + ' не был загружен. Error: Not a valid file format!');

            if(file.size > APP.maxFileSize)
                return window.modal.openError('Файл '
                    + file.name + ' не был загружен. Error: The file size should not exceed'
                    + APP.maxFileSize / 1048576
                    + ' megabytes');


            var reader = new FileReader();
            reader.onload = function (e) {

                var result = {
                    id: id,
                    name: file.name,
                    type: file.type,
                    src: e.target.result
                }

                self.props.uploadedFile(result);
            }
            reader.readAsDataURL(file);

            window.api.uploadFile(file,
                function (r) {

                    try{
                        var res = JSON.parse(r);
                    }catch (e){
                        window.modal.openError('Файл ' + file.name + ' не был загружен. Не известная ошибка');
                        console.log(e);
                        return self.props.notUploadedFile(id);
                    }

                    if(res.type == 'error'){
                        window.modal.openError('Файл ' + file.name + ' не был загружен. Error: ' + res.error);
                        return self.props.notUploadedFile(id);
                    }

                    var data = res.data[0];
                    var selectedFile = self.props.selectedFile;

                    APP.files.unshift(data);
                    self.props.fileUploaded(id, data);

                    if(selectedFile.uploading && selectedFile.id == id){
                        self.props.selectedFile(data.id, data.name, false)
                    }
                },function (res) {
                    console.log(res);
                    window.modal.openError('Файл ' + file.name + ' не был загружен. Не известная ошибка, попробуйте позже');
                    return self.props.notUploadedFile(id);
                }
            );

        }

        for(var i=0; i<files.length; i++){
             upload(files[i], i);
        }

        self.props.goToPage('default');

    }

    dragOver(e){
        e.preventDefault();
        $(this.refLoadArea.current).show();
    }

    dragLeave(e){
        $(this.refLoadArea.current).hide();
    }

    drop(e){
        event.preventDefault();
        $(this.refLoadArea.current).hide();
        this.changeFiles(e.dataTransfer.files);
    }

    render(){

        return (
            <div className="file-manager__center">
                <div className="fileManagerContent fileManagerContent-full fileManagerContent-notScroll relative"
                     onDragOver={this.dragOver}>
                    <div className="table">
                        <div className="table-cell">
                            <div className="fm-blockUpload">
                                <p className="fm-blockUpload__title fm-header">Выберете файл или перетяните его в эту область</p>
                                <button onClick={this.addUploadFiles} className="btnDefault btnDefault-big">Выбрать файл</button>
                                <input onChange={this.changeFiles} ref={this.refInputUpload} type="file" style={{display: 'none'}} accept={this.getFileTypes()} multiple={true} />
                            </div>
                        </div>
                    </div>
                    <div ref={this.refLoadArea} className="loadArea"
                         onDragLeave={this.dragLeave} onDrop={this.drop}>
                        <div className="loadAreaContent"></div>
                    </div>
                </div>
            </div>
        )

    }

}

function mapStateToProps(state){
    return state;
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        uploadedFile: uploadedFile,
        fileUploaded: fileUploaded,
        goToPage: goToPage,
        selectedFile: selectedFile,
        notUploadedFile: notUploadedFile
    },dispatch)
}

export default connect(mapStateToProps,matchDispatchToProps)(ContentUpload);