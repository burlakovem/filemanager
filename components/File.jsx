/**
 * File
 */

import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {selectFile} from '../actions/index';


class File extends React.Component{

    constructor(props){
        super();
    }

    /**
     * Icon rendering for selected file
     * @return {XML}
     */
    renderSelectedIcon(){
        if(this.props.selected){
            return <i className="fm-selectedIcon"></i>
        }
    }

    /**
     * Preloader rendering
     * @return {XML}
     */
    renderPreloader(){
        if(this.props.uploading){
            return <div className="fm-contentBlock__preloader"></div>
        }
    }

    /**
     * Getting file icon
     * @param src
     * @param type
     * @return {*}
     */
    getFileIcon(src, type){

        if(APP.allowedArchiveUpload.search(type) != -1){
            return '/img/archive.png';
        }

        return src;
    }

    render(){
        return(
            <div onClick={() => this.props.selectFile(this.props.fileId, this.props.name, this.props.uploading)} className="fm-contentBlock fm-contentBlock-small fileManagerContent__block chessboard-texture relative">

                {this.renderPreloader()}

                <div className="table">
                    <div className="table-cell">
                        <img className="fm-contentBlock__img" src={
                            this.getFileIcon(this.props.src, this.props.type)
                        } alt={this.props.alt} />
                    </div>
                </div>

                {this.renderSelectedIcon()}

            </div>
        );
    }

}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        selectFile: selectFile
    },dispatch)
}

export default connect(null, matchDispatchToProps)(File);