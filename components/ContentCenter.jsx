/**
 * Content center
 */

import React from 'react';
import ContentContainer from '../containers/ContentContainer';


class ContentCenter extends React.Component {

    render() {
        return(
            <div className="file-manager__center">
                <ContentContainer />
            </div>
        )
    }

}

export default ContentCenter;