/**
 * Content left
 */

import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {goToPage} from '../actions/index';


class ContentLeft extends React.Component{

    constructor(props){
        super();
    }

    /**
     * Rendering btns and marking btn this page
     * @return {XML}
     */
    renderBtns(){

        var btns = (
            <div className="file-manager__left">
                <button data-id='default' onClick={() => this.props.goToPage('default')} className="btnDefault btnDefault-notActive">Выбор файла</button>
                <button data-id='upload' onClick={() => this.props.goToPage('upload')} className="btnDefault btnDefault-notActive">Загрузить</button>
            </div>
        )

        var btns_children = btns.props.children;

        for(var i=0; i<btns_children.length; i++){
            var btn = btns_children[i].props;
            if(btn['data-id'] == this.props.currentPage){
                btn.className = btn.className.replace('btnDefault-notActive');
            }
        }

        return btns;

    }

    render(){
        return this.renderBtns()
    }

}

function mapStateToProps(state){
    return state;
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        goToPage: goToPage
    },dispatch)
}

export default connect(mapStateToProps,matchDispatchToProps)(ContentLeft);