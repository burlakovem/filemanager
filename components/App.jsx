/**
 * Root render
 */

import React from 'react';
import {connect} from 'react-redux';
import ContentTop from './ContentTop';
import ContentLeft from './ContentLeft';
import ContentCenter from './ContentCenter';
import ContentRight from './ContentRight';
import ContentUpload from './ContentUpload';


class App extends React.Component{

    constructor(props){
        super();
    }

    renderContentCenter(){
        if(this.props.currentPage == 'default'){
            return <ContentCenter />
        }
    }

    renderContentRight(){
        if(this.props.currentPage == 'default'){
            return <ContentRight />
        }
    }

    renderContentUpload(){
        if(this.props.currentPage == 'upload'){
            return <ContentUpload />
        }
    }

    render(){

        return(
            <div className="file-manager">
                <span onClick={() => this.props.close()} className="file-manager__close"></span>

                <ContentTop />

                <ContentLeft />

                {this.renderContentCenter()}

                {this.renderContentRight()}

                {this.renderContentUpload()}

                <div class="clearfix"></div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return state;
}

export default connect(mapStateToProps)(App);