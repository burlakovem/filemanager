/**
 * Content right
 */

import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {saveParameter, deleteFile} from '../actions/index';


class ContentRight extends React.Component{

    constructor(props){
        super();
        this.copySrc = this.copySrc.bind(this);

        this.deleteFile = this.deleteFile.bind(this);

        // For title field
        this.refTitleInput = React.createRef();
        this.startEditingTitle = this.startEditingTitle.bind(this);
        this.endEditingTitle = this.endEditingTitle.bind(this);
        this.saveTitle = this.saveTitle.bind(this);

        // For alt field
        this.refAltInput = React.createRef();
        this.startEditingAlt = this.startEditingAlt.bind(this);
        this.endEditingAlt = this.endEditingAlt.bind(this);
        this.saveAlt = this.saveAlt.bind(this);

        this.state = {
            editingAlt: false,
            editingTitle: false
        }
    }

    /**
     * Delete file
     */
    deleteFile(){
        var self = this;
        var file = self.file;

        window.api.deleteFile(
            file.name,
            function (res) {

                try{
                    var data = JSON.parse(res);
                }catch (e){
                    console.log(e);
                    return window.modal.openError('Не известная ошибка, попробуйте позже');
                }

                if(data.type == 'ok'){
                    APP.files = APP.files.filter(function (item) {
                        if(item.name !== file.name) return true;
                    });
                    return self.props.deleteFile(file.name);
                }

                window.modal.openError('Не известная ошибка, попробуйте позже');
                console.log(data.error);

            },function (res) {
                console.log(res);
                window.modal.openError('Не известная ошибка, попробуйте позже');
            }
        )
    }

    /**
     * Saving file parameter
     * @param field
     * @param value
     */
    saveParameter(field, value){

        var self = this;
        var beforeValue = this.file.params[field];
        var name = this.file.name;

        window.api.updateFileParameter(
            name,
            field,
            value,
            function (res) {

                try{
                    var data = JSON.parse(res);
                }catch (e){
                    console.log(e);
                    self.props.saveParameter(name, field, beforeValue);
                    return window.modal.openError('Не известная ошибка, параметры файла не обновлены');
                }

                if(data.type == 'error'){
                    self.props.saveParameter(name, field, beforeValue);
                    return window.modal.openError(res.error);
                }
                APP.files = APP.files.map(function (item) {
                    if(item.name == name){
                        item.params[field] = value;
                    }
                    return item;
                })
            },function (res) {
                console.log(res);
                window.modal.openError('Не известная ошибка, попробуйте позже');
            }
        );
        this.props.saveParameter(name, field, value);
    }

    /**
     * Begin editing title
     */
    startEditingTitle(){
        this.setState((prevState, props) => ({
            editingTitle: true
        }))
    }

    /**
     * End editing title
     */
    endEditingTitle(){
        this.setState((prevState, props) => ({
            editingTitle: false
        }))
    }

    /**
     * Save title
     */
    saveTitle(){
        this.saveParameter('title', this.refTitleInput.current.value);
        this. endEditingTitle();
    }

    /**
     * Title rendering
     * @return {XML}
     */
    renderTitle(){
        var params = this.file.params;

        if(this.state.editingTitle){

            return (
                <p className="fm-aboutField">

                    <input ref={this.refTitleInput} className="fm-aboutField__input" type="text" defaultValue={params.title}/>

                    <a onClick={this.saveTitle} href="javascript:void(0);" className="fieldsLink">Сохранить</a>&emsp;

                    <a onClick={this.endEditingTitle} href="javascript:void(0);" className="fieldsLinkDelete">Отмена</a>

                </p>
            )

        }

        return (
            <p onClick={this.startEditingTitle} className="fm-aboutField editableField relative">
                <span class="fm-aboutField__title">Title -</span>
                {params.title}
                <i class="editableFieldIcon editableFieldIcon-small"></i>
            </p>
        )

    }

    /**
     * Begin editing alt
     */
    startEditingAlt(){
        this.setState((prevState, props) => ({
            editingAlt: true
        }))
    }

    /**
     * End editing alt
     */
    endEditingAlt(){
        this.setState((prevState, props) => ({
            editingAlt: false
        }))
    }

    /**
     * Save alt
     */
    saveAlt(){
        this.saveParameter('alt', this.refAltInput.current.value);
        this. endEditingAlt();
    }

    /**
     * Alt rendering
     * @return {XML}
     */
    renderAlt(){
        var params = this.file.params;

        if(this.state.editingAlt){

            return (
                <p className="fm-aboutField">

                    <input ref={this.refAltInput} className="fm-aboutField__input" type="text" defaultValue={params.alt}/>

                    <a onClick={this.saveAlt} href="javascript:void(0);" className="fieldsLink">Сохранить</a>&emsp;

                    <a onClick={this.endEditingAlt} href="javascript:void(0);" className="fieldsLinkDelete">Отмена</a>

                </p>
            )

        }

        return (
            <p onClick={this.startEditingAlt} className="fm-aboutField editableField relative">
                <span class="fm-aboutField__title">Alt -</span>
                {params.alt}
                <i class="editableFieldIcon editableFieldIcon-small"></i>
            </p>
        )

    }


    /**
     * Fields rendering for this file type
     * @return {XML}
     */
    renderFileParameters(){
        if(this.file.uploading) return;

        var params = this.file.params;
        var result = [];

        for(var key in params){
            result.push(this['render' + key.charAt(0).toUpperCase() + key.substr(1)]());
        }
        return result;
    }

    /**
     * Method copy src for this file
     */
    copySrc(src){
        var input = $('<input type="text" value="' + src + '"/>');
        $(document.body).append(input);
        input.select();
        document.execCommand('copy');
        input.detach();
    }

    /**
     * Top info rendering
     * @return {XML}
     */
    renderFileTop(){
        var file = this.file;
        var src = file.src;

        if(APP.allowedArchiveUpload.search(file.type) != -1){
            src = '/img/archive.png';
        }

        return (
            <div>
                <p className="fm-imgInfo__title">{file.name}</p>
                <div onClick={() => window.open(file.src)} className="fm-imgInfo__block fm-contentBlock chessboard-texture relative">

                    {this.renderPreloader()}

                    <div className="table">
                        <div className="table-cell">
                            <img className="fm-contentBlock__img" src={src} alt="file" />
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Default parameters for any files
     * @return {XML}
     */
    renderDefaultParams(){

        if(this.props.selectedFile.uploading) return;

        return (
            <div>
                <p className="fm-aboutField relative">
                    <a onClick={() => this.copySrc(location.origin + this.file.src)} className="fieldsLink" href="javascript:void(0);">Копировать адрес</a>
                </p>
                <p className="fm-aboutField relative">
                    <a onClick={this.deleteFile} className="fieldsLinkDelete" href="javascript:void(0);">Удалить</a>
                </p>
            </div>
        )

    }

    /**
     * File actions
     * @return {XML}
     */
    renderFileActions(){

        if(this.props.selectedFile.uploading) return;

        return (
            <div className="fm-resultField">
                <button onClick={() => this.props.close(this.file.name)} className="btnDefault btnDefault-big">Выбрать файл</button>
                <button onClick={() => this.props.close()} className="btnDefault btnDefault-big btnDefault-notActive">Отмена</button>
            </div>
        )

    }

    /**
     * Preloader rendering
     * @return {XML}
     */
    renderPreloader(){
        if(this.props.selectedFile.uploading){
            return <div className="fm-contentBlock__preloader fm-contentBlock__preloader-big"></div>
        }
    }

    render(){

        if(this.props.selectedFile.uploading){
            var files = this.props.uploadingFiles;
        }else{
            var files = this.props.files;
        }

        for(var id = 0; id<files.length && this.props.selectedFile; id++){
            if(files[id].name == this.props.selectedFile.name){
                break;
            }
        }

        if(id >= files.length){

            return <div className="file-manager__right"></div>

        }

        this.file = files[id];

        return(
            <div className="file-manager__right">
                <div className="fm-imgInfo">

                    {this.renderFileTop()}

                    {this.renderFileParameters()}

                    {this.renderDefaultParams()}

                </div>

                {this.renderFileActions()}

            </div>
        )

    }

}

function mapStateToProps(state){
    return state;
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        saveParameter: saveParameter,
        deleteFile: deleteFile
    },dispatch)
}

export default connect(mapStateToProps,matchDispatchToProps)(ContentRight);