/**
 * Content top
 */

import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {searchKeyUp} from '../actions';


class ContentTop extends React.Component{

    constructor(props){
        super();
        this.refInput = React.createRef();
    }

    renderHeader(){
        if(this.props.currentPage == 'upload'){
            return <p class="fm-header">Загрузите файлы</p>
        }
        return (
            <div className="searchField">
                <input ref={this.refInput} onKeyUp={() => this.props.searchKeyUp(this.refInput.current.value)} className="searchField__input" type="text" placeholder="Поиск"/>
                <img className="searchField__decor" src="/img/search.png" alt="search"/>
            </div>
        );
    }

    render(){
        return(
            <div className="file-manager__top">
                {this.renderHeader()}
            </div>
        )
    }
}

function mapStateToProps(state){
    return state;
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        searchKeyUp: searchKeyUp
    },dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(ContentTop);