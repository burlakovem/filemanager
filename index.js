/**
 * File manager
 */

import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import {combineReducers} from 'redux';
import files from './reducers/files';
import currentPage from './reducers/currentPage';
import uploadingFiles from './reducers/uploadingFiles';
import searchValue from './reducers/searchValue';


/**
 * Id for file manager modal window
 * @type {string}
 */
const FILE_MANAGER_ID = 'fileManager';

/**
 * Id for file manager root
 * @type {string}
 */
const FILE_MANAGER_ROOT_ID = FILE_MANAGER_ID + '__root';

window.fileManager = {

    /**
     * Function for starting app
     * @param fileTypes
     * @param callback
     * @param selectedName
     */
    open: function(fileTypes = 'any',
                   callback = function (data) {console.log(data)},
                   selectedName = false
    ){
        var self = this;
        this.callback = callback;


        /**
         * Combined reducers
         * @type {Reducer<S>|Reducer<S, A>}
         */
        const ALL_REDUCERS = combineReducers({
            files: files,
            fileTypes: function () {
                return (function (state = null) {
                    return fileTypes;
                })()
            },
            close: function () {
                return function (state = null, data = false) {
                    var data = (typeof state == 'string')?state:data;
                    return self.close(data);
                }
            },
            selectedFile: function (state = null, action) {

                return (function (state = null, action) {

                    switch (action.type){

                        case 'SELECT_FILE':
                            return {
                                id: action.id,
                                name: action.name,
                                uploading: action.uploading
                            };

                        default:
                            return state || {
                                    id: '-1',
                                    name: selectedName,
                                    uploading: false
                                }

                    }

                })(state, action)
            },
            currentPage: currentPage,
            uploadingFiles: uploadingFiles,
            searchValue: searchValue
        });

        /**
         * App store
         */
        const STORE = createStore(ALL_REDUCERS);


        /**
         * jQuery file manager object
         * @type {Element}
         */
        const FILE_MANAGER = $("#" + FILE_MANAGER_ID);

        /**
         * Root object
         * @type {Element}
         */
        const ROOT = document.getElementById(FILE_MANAGER_ROOT_ID);

        FILE_MANAGER.fadeIn();

        ReactDOM.render(
            <Provider store={STORE}>
                <App />
            </Provider>,
            ROOT
        );
    },

    close: function (data = false) {
        /**
         * jQuery file manager object
         * @type {Element}
         */
        const FILE_MANAGER = $("#" + FILE_MANAGER_ID);

        /**
         * Root object
         * @type {Element}
         */
        const ROOT = document.getElementById(FILE_MANAGER_ROOT_ID);

        FILE_MANAGER.fadeOut();

        ReactDOM.unmountComponentAtNode(
            ROOT
        );

        this.callback(data);
    }
}