
export const selectFile = function (id, name, uploading = false) {
    return {
        type: "SELECT_FILE",
        id: id,
        name: name,
        uploading: uploading
    }
}

export const saveParameter = function (name, field, value) {
    return {
        type: "SAVE_PARAMETER",
        name: name,
        field: field,
        value: value
    }
}

export const goToPage = function (id) {
    return {
        type: "GO_TO_PAGE",
        id: id
    }
}

export const uploadedFile = function (file) {
    return {
        type: "UPLOADED_FILE",
        file: file
    }
}

export const fileUploaded = function (id, file) {
    return {
        type: "FILE_UPLOADED",
        id: id,
        file: file
    }
}

export const deleteFile = function (name) {
    return {
        type: "DELETE_FILE",
        name: name
    }
}

export const notUploadedFile = function (name) {
    return {
        type: "NOT_UPLOADED_FILE",
        id: name
    }
}

export const searchKeyUp = function (val) {
    return {
        type: "SEARCH_KEY_UP",
        val: val
    }
}