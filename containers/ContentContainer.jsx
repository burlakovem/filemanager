/**
 * Container for files
 */

import React from 'react';
import {connect} from 'react-redux';
import File from '../components/File';


class ContentContainer extends React.Component{

    constructor(props){
        super();
    }

    /**
     * Check select file
     * @param item
     * @return {boolean}
     */
    checkSelected(item){
        return item.name === this.props.selectedFile.name
            && (item.uploading || false) == this.props.selectedFile.uploading;
    }

    /**
     * Creating file list
     * @return {Array}
     */
    renderFiles(){
        var self = this;
        var types = this.props.fileTypes;
        var files = [...this.props.files];
        var uploadingFiles = [...this.props.uploadingFiles].map(function (item) {
            item.uploading = true;
            return item;
        });

        var list = uploadingFiles.concat(files);
        var regExp = new RegExp(this.props.searchValue, 'gi');

        list = list.filter(function (item) {
            var result = false;
            if(types == 'any') result = true;
            if(typeof types == 'string'){
                if(item.type == types){
                    result = true;
                }
            }else{
                if(types.indexOf(item.type) != -1){
                    result = true;
                }
            }

            if(item.name.search(regExp) != -1) return result;

            if(item.params.title && item.params.title.search(regExp) != -1) return result;

            if(item.params.alt && item.params.alt.search(regExp) != -1) return result;

        });
        return list.map(function(item,id) {
            return (
                <File id={id} key={id} type={item.type} name={item.name}
                      src={item.src} params={item.params} fileId={item.id}
                      uploading={item.uploading || false} selected={self.checkSelected(item)}/>
            );
        });
    }

    render(){
        return(
            <div className="fileManagerContent">
                {this.renderFiles()}
            </div>
        )

    }

}

function mapStateToProps(state){
    return state;
}

export default connect(mapStateToProps)(ContentContainer);