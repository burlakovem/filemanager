import {copyFiles} from '../functions';

export default function () {
    return copyFiles(APP.files);
}