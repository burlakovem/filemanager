export default function (state = null, action) {

    switch (action.type){

        case 'SEARCH_KEY_UP':
            var value = action.val;
            value = value.replace(/\/|\\/g, '');
            return value;

        default:
            return state || '';

    }

}