/**
 * Get current page id
 * @param state
 * @param action
 * @return {*|string}
 */


export default function (state = null, action) {

    switch (action.type){

        case 'GO_TO_PAGE':
            return action.id;

        default:
            return state || 'default';

    }

}