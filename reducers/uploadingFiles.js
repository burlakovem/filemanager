/**
 * Uploading files reducer
 */

import {copyFiles} from '../functions';


export default function (state = null, action) {

    switch (action.type){

        case 'UPLOADED_FILE':
            var files = copyFiles(state);
            files.push(action.file);
            return files;

        case 'FILE_UPLOADED':
            var files = copyFiles(state);
            files = files.filter(function (item) {
                if(item.id !== action.id){
                    return true;
                }
            });
            return files;

        case 'NOT_UPLOADED_FILE':
            var files = copyFiles(state);
            files = files.filter(function (item) {
                if(item.id !== action.id){
                    return true;
                }
            });
            return files;

        default:
            return state || [];

    }

}