/**
 * Files reducer
 */

import FilesReducer from './FilesReducer';
import {copyFiles} from '../functions';


/**
 * Get files
 * @param state
 * @param action
 * @return {*}
 */
export default function (state=null, action) {
    switch (action.type){

        case 'SAVE_PARAMETER':
            var files = copyFiles(state);

            files = files.map(function (item) {
                if(item.name == action.name){
                    item.params[action.field] = action.value;
                }
                return item;
            });

            return files;

        case 'SET_FILES':
            return action.data;

        case 'FILE_UPLOADED':
            var files = copyFiles(state);
            files.unshift(action.file);
            return files;

        case 'DELETE_FILE':
            var files = copyFiles(state);
            files = files.filter(function (item) {
                if(item.name != action.name) return true;
            });
            return files;

        default:
            return state || FilesReducer();

    }

}